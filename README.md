# MARS ROVER MISSION

## 1. Explicación

### 1.1 Reto

El reto consistía en elaborar un supuesto sistema de guía para el rover de marte. Que recibiera una orientación y un seguido de comandos que guiaran su movimiento y comportamiento. Además, tenía que tener un sistema de detección de obstáculos para evitar colisiones.

Todo el proceso se ha llevado a cabo intentando seguir el patrón de diseño hexagonal con DDD con un fuerte tipado gracias a Typescript y estructurado por clases dejando a la capa de infraestructura de la aplicación que se elaborara siguiendo una estructura funcional para que compaginará de una forma eficaz y sencilla con el framework ReactJS.

Otro punto a tener en cuenta es que se ha intentado elaborar el proyecto paraa que sea escalable y ante la posible integración de una base de datos real en el futuro, basada
en el consumo de una API Rest. Además, se ha utilizado React-redux para tener una store funcional y permitiera una mejor reactividad de la UI del proyecto.

Para la realización de los tests se ha decidido usar jest, ya que es una buena librería para realizar test unitarios y de integración.

Por último, se ha utilizado Git para el respaldo del código y se ha utilizado una estructura feature-based para la integración de distintas ramas del proyecto a la rama main.

### 1.2 Tecnologías

Se han usado las siguientes tecnologías para llevar a cabo el reto:
- NodeJS
- ReactJS
- Typescript
- React-redux
- Jest
- HTML5
- CSS
- sweetalert2

### 1.3 Estado del reto

En el estado actual en que se encuentra el proyecto, estos serían los puntos a mejorar o que faltarían por integrar:
- Tests de integración: en este punto no són muy necesarios ya que no tenemos una base de datos real con la que trabajar.
- Test de aceptación: No se han llevado a cabo por falta de tiempo.
- Mejora de la UI: mejorar la interfaz de usuario para su integración.
- Mejora del sistema de previsión de colisiones: habría que reajustar el algoritmo de detección para un mejor funcionamiento en base a su objetivo.
- Mejora del sistema de notificaciones: habría que mejorar el sistema de notificaciones para alertar mejor al usuario de una posible colisión o de la
nueva entrada de comandos.

## 2. Ejecución

Para ejecutar el proyecto existen las dos siguientes opciones:

### A) Node

Se puede ejecutar el proyecto con la versión 14.17.6 de Node instalada.
Solo se debería ejecutar la orden "npm install" en la terminal desde la carpeta raíz y acto seguido "npm start", esto hará que se compilen los archivos Typescript y se ejecute la aplicación y a través de la dirección http://localhost:3000 se puede acceder a ella.

### B) Docker

También existe la opción de ejecutar el proyecto con docker vía docker-compose. La orden es "docker-compose up". Esto permite recibir parametros por la misma consola con la que ejecutamos docker una vez se han compilado los archivos TS.

### 3 Testing

#### A) Node

Se puede ejecutar el testing con la versión 14.17.6 de Node instalada.
Solo se debería ejecutar la orden "npm install" en la terminal desde la carpeta raíz y acto seguido "npm run test:unit", esto hará que se ejecuten todos los tests unitarios.

