import React from 'react';
import './App.css';
import { StartPage } from './start-page';

const App: React.FC = () => {
  return (
    <div className="App">
      <StartPage />
    </div>
  );
}


export default App;
