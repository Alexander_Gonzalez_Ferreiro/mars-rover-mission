import * as React from 'react'
import './index.css';
import MarsImage from '../../../../assets/images/marte.jpg';
import RoverImage from '../../../../assets/images/rover2.png';
import RockImage from '../../../../assets/images/rock2.png';
import { Dispatch } from "redux"
import { useDispatch } from "react-redux"
import { ICanvas } from '../../type';
import { RoverService } from '../../services/RoverService';

type Props = {
    canvas: ICanvas,
    saveCanvas: (position: Array<number>) => void,
    dataMars: any
}

const SCALE: number = 3;

export const Canvas: React.FC<Props> = ({ canvas, saveCanvas, dataMars }) => {
    const EXAMPLE_MOVEMENT = dataMars.movement;
    const [indexMovement, setIndexMovement] = React.useState(0);
    const dispatch: Dispatch<any> = useDispatch();
    const canvasRef = React.useRef<HTMLCanvasElement>(null);

    const canvasSaver = React.useCallback(
        (position: Array<number>) => dispatch(saveCanvas(position)),
        [dispatch, saveCanvas]
    )

    React.useEffect(() => {
        if (canvasRef.current) {
            const canvasMap = canvasRef.current;
            const canvasTag = document.getElementsByTagName('canvas')[0];
            canvasTag.setAttribute('width', window.innerWidth.toString());
            canvasTag.setAttribute('height', window.innerHeight.toString());
            canvasRef.current.width = Math.floor(window.innerWidth * SCALE);
            canvasRef.current.height = Math.floor(window.innerHeight * SCALE);
            window.devicePixelRatio = SCALE;
            const context = canvasMap.getContext('2d');
            context?.scale(SCALE, SCALE);
            context?.translate(-(canvas.rover.coordinates.positionX.value) + (canvasRef.current.width / (SCALE * 6)), -(canvas.rover.coordinates.positionY.value) + (canvasRef.current.height / (SCALE * 6)));

            context?.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);
            context?.strokeRect(0, 0, canvasRef.current.width, canvasRef.current.height);

            const imageObj = new Image();
            imageObj.src = RoverImage;
            const scaleWidth = canvasRef.current.width / 250;
            const scaleHeight = canvasRef.current.height / 250;
            imageObj.onload = function () {
                context?.drawImage(imageObj, canvas.rover.coordinates.positionX.value, canvas.rover.coordinates.positionY.value, scaleWidth, scaleHeight);
                const imageRock = new Image();
                imageRock.src = RockImage;
                imageRock.onload = function () {
                    canvas.obstacles?.obstacles.forEach((obj) => {
                        context?.drawImage(imageRock, obj.coordinates.positionX.value, obj.coordinates.positionY.value, scaleWidth, scaleHeight)
                    });
                }
            }
        }
        const interval = setInterval(() => {
            const positionX = canvas.rover.coordinates.positionX.value;
            const positionY = canvas.rover.coordinates.positionY.value;
            const position = RoverService.calculateMovement(EXAMPLE_MOVEMENT[indexMovement], [positionX, positionY], canvas.rover.direction.value);
            const newIndex = indexMovement + 1;
            if (!canvas.collapse && position) {
                canvasSaver([position[0], position[1]]);
            }
            if (newIndex <= EXAMPLE_MOVEMENT.length) {
                setIndexMovement(newIndex);
            }
        }, 2000);

        return () => clearInterval(interval);
    }, [canvas,canvasSaver, indexMovement, EXAMPLE_MOVEMENT]);
    return (
        <div className="background" style={{ backgroundImage: `linear-gradient(black, black), url(${MarsImage})`, backgroundBlendMode: "saturation" }}>
            <canvas ref={canvasRef} />
        </div>
    );
};