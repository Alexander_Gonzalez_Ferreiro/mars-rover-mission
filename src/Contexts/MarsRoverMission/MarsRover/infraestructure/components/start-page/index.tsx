import * as React from 'react'
import './index.css';
import MarsImage from '../../../../assets/images/marte.jpg';
import { StartForm } from '../start-form';
import { MarsMissionPage } from '../mars-mission-page';

export const StartPage: React.FC<any> = () => {
    const [finalizedForm, setFinalizedForm] = React.useState(false);
    const [dataMars, setDataMars] = React.useState(null);

    return (
        <div id="start-page" style={{ backgroundImage: `url(${MarsImage})` }}>
            {finalizedForm ? <MarsMissionPage dataMars={dataMars} /> : <StartForm setFinalizedForm={setFinalizedForm} setDataMars={setDataMars} />}
        </div>
    );
}