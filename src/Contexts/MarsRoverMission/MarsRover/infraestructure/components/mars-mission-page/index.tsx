import * as React from 'react'
import './index.css';
import { Canvas } from '../background';
import { useSelector, shallowEqual, useDispatch } from "react-redux"
import { Dispatch } from "redux"
import { CanvasRoverState, ICanvas } from '../../type';
import { createCanvas, saveCanvas } from '../../store/actionCreators';

export const MarsMissionPage: React.FC<any> = ({ dataMars }) => {
    const canvas: ICanvas = useSelector(
        (state: CanvasRoverState) => state.canvas,
        shallowEqual
    )

    const dispatch: Dispatch<any> = useDispatch();

    React.useEffect(() => {
        dispatch(createCanvas(dataMars));
    }, [dispatch, dataMars]);

    return (
        <div id="mars-mission-page">
            <Canvas canvas={canvas} saveCanvas={saveCanvas} dataMars={dataMars} />
        </div>
    );
}