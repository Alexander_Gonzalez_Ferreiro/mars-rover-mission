import * as React from 'react'
import './index.css';

export const StartForm: React.FC<any> = ({ setFinalizedForm, setDataMars }) => {


    const [data, setData] = React.useState({
        movement: '',
        direction: 'North'
    })

    const handleInputChange = (event: React.FormEvent<HTMLInputElement>) => {
        setData({
            ...data,
            [event.currentTarget.name]: event.currentTarget.value
        })
    }

    const enviarDatos = (event: React.FormEvent) => {
        event.preventDefault();
        const result = {
            ...data,
            movement: data.movement.trim().split(',')
        };
        setDataMars(result);
        setFinalizedForm(true);
    }

    const handleClickButton = () => {
        setData({
            ...data,
            movement: 'f, f, r, r, f, f, f, r, l'
        })
    }

    const handleSelectChange = (event: React.FormEvent<HTMLSelectElement>) => {
        setData({
            ...data,
            direction: event.currentTarget.value
        })
    }

    return (
        <React.Fragment>
            <form className="form" onSubmit={enviarDatos}>
                <h1 className='form-title'>Empieza la aventura</h1>
                <div className="form-input">
                    <input type="text" placeholder="Movimientos" className="form-control" onChange={handleInputChange} name="movement" value={data.movement} required></input><br />
                    <span className='input-span'>* Separados por coma y tiene que ser uno de los siguientes (f), (l) o (r)</span>
                    <button className='generate-movement' onClick={handleClickButton}>Auto generar movimiento</button>
                </div>
                <div className="form-input">
                    <select className="form-control" onChange={handleSelectChange} defaultValue={"north"}>
                        <option value="north">North</option>
                        <option value="east">East</option>
                        <option value="west">West</option>
                        <option value="south">South</option>
                    </select>
                </div>
                <button type="submit">Enviar</button>
            </form>
        </React.Fragment>
    );
}