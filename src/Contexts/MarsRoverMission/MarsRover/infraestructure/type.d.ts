import { CollectionObstacles } from "../domain/CollectionObstacles";
import { Rover } from "../domain/Rover";

interface ICanvas {
    rover: Rover;
    obstacles: CollectionObstacles;
    collapse: boolean;
}

type CanvasRoverState = {
    canvas: ICanvas
}

type CanvasAction = {
    type: string
    canvas: ICanvas
}

type DispatchType = (args: CanvasAction) => CanvasAction;