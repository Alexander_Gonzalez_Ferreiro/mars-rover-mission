import { CollectionObstacles } from "../../domain/CollectionObstacles";
import { Coordinates } from "../../domain/Coordinates";
import { Obstacle } from "../../domain/Obstacle";
import { Rover } from "../../domain/Rover";
import { RoverRepository } from "../../domain/RoverRepository";
import { Direction } from "../../domain/ValueObjects/Direction";
import { RoverId } from "../../domain/ValueObjects/RoverId";

export class InMemoryRoverRepository implements RoverRepository {
    private static rovers: Array<Rover> = [];
    private static obstacles: CollectionObstacles = CollectionObstacles.createEmpty();

    async createRover(roverId: RoverId, coordinates: Coordinates, direction: Direction): Promise<void> {
        return new Promise((resolve, reject) => {
            const rover: Rover = Rover.create(roverId, coordinates, direction);
            InMemoryRoverRepository.rovers.push(rover);
            resolve();
        });

    };


    async findRover(roverId: RoverId): Promise<Rover> {
        return new Promise((resolve, reject) => {
            const rover: Rover[] = InMemoryRoverRepository.rovers.filter(
                (o) => o.id.value === roverId.value
            );

            return resolve(rover[0]);

        });
    };

    async createObstacle(coordinates: Coordinates): Promise<void> {
        return new Promise((resolve, reject) => {
            const obstacle: Obstacle = Obstacle.create(coordinates);
            InMemoryRoverRepository.obstacles.addObstacle(obstacle);
            resolve();
        });

    };

    async searchObstacles(): Promise<CollectionObstacles> {
        return new Promise((resolve, reject) => {
            resolve(InMemoryRoverRepository.obstacles);
        });
    };

    async roverExists(roverId: RoverId): Promise<boolean> {
        const rover: Rover | undefined = InMemoryRoverRepository.rovers.find(
            (o) => o.id.value === roverId.value
        );
        if (!(rover instanceof Rover)) return false;

        return true;
    }

    async existsObstacles(): Promise<Boolean> {
        if (InMemoryRoverRepository.obstacles.obstacles.length < 1) return false;

        return true;
    }

    async existObstacle(coordinates: Coordinates): Promise<Boolean> {
        const obstacle: Obstacle | undefined = InMemoryRoverRepository.obstacles.obstacles.find(
            (o) => o.coordinates.positionX.value === coordinates.positionX.value && o.coordinates.positionY.value === coordinates.positionY.value
        );
        if (obstacle instanceof Obstacle) return true;

        const rover: Rover | undefined = InMemoryRoverRepository.rovers.find(
            (o) => o.coordinates.positionX.value === coordinates.positionX.value && o.coordinates.positionY.value === coordinates.positionY.value
        );
        if (rover instanceof Rover) return true;

        return false;
    };

    async saveRover(roverId: RoverId, coordinates: Coordinates): Promise<void> {
        return new Promise((resolve, reject) => {
            InMemoryRoverRepository.rovers.forEach(
                (o) => {
                    if (o.id.value === roverId.value) { o.updateCoordinates(coordinates) }
                }
            );
            resolve();
        });
    }
}