import { CreateObstacleUseCase } from '../../application/CreateObstacleUseCase';
import { GetAllObstaclesUseCase } from '../../application/GetAllObstaclesUseCase';
import { ObstacleCreator } from '../../domain/create/ObstacleCreator';
import { ObstacleSearcher } from '../../domain/searcher/ObstacleSearcher';
import { InMemoryRoverRepository } from '../persistence/InMemoryRoverRepository';

export class ObstacleService {
  static async create(num: number) {
    try {
      const createObstacleUseCase = new CreateObstacleUseCase(new ObstacleCreator(new InMemoryRoverRepository()));
      await createObstacleUseCase.run(num);

    } catch (error) {
      let message = 'Unknown Error';
      if (error instanceof Error) message = error.message;
      console.error(message);
    }
  }

  static async getAll() {
    try {
      const getObstacleUseCase = new GetAllObstaclesUseCase(new ObstacleSearcher(new InMemoryRoverRepository()));

      const obstacles = await getObstacleUseCase.run();
      return obstacles;
    } catch (error) {
      let message = 'Unknown Error';
      if (error instanceof Error) message = error.message;
      console.error(message);
    }
  }

}