import { CreateRoverUseCase } from '../../application/CreateRoverUseCase';
import { DetectObstaclesUseCase } from '../../application/DetectObstaclesUseCase';
import { RoverDTO } from '../../application/DTO/RoverDTO';
import { RoverUpdateDTO } from '../../application/DTO/RoverUpdateDTO';
import { GetRoverUseCase } from '../../application/GetRoverUseCase';
import { UpdateRoverUseCase } from '../../application/UpdateRoverUseCase';
import { RoverCreator } from '../../domain/create/RoverCreator';
import { RoverFinder } from '../../domain/finder/RoverFinder';
import { RoverSave } from '../../domain/save/RoverSave';
import { CollapseSearcher } from '../../domain/searcher/CollapseSearcher';
import { InMemoryRoverRepository } from '../persistence/InMemoryRoverRepository';
import Swal from 'sweetalert2'

const CASILLA_SCALE: number = 200;
export class RoverService {
    private static roverId = Math.floor(Math.random() * (200 - 0));

    static async create(dataMars: any, width: number, height: number) {
        try {
            const roverDto = RoverDTO.create(this.roverId, width, height, dataMars.direction)
            const createRoverUseCase: CreateRoverUseCase = new CreateRoverUseCase(new RoverCreator(new InMemoryRoverRepository()));
            await createRoverUseCase.run(roverDto);

        } catch (error) {
            let message = 'Unknown Error';
            if (error instanceof Error) message = error.message;
            console.error(message);
        }
    }

    static async getRover() {
        try {
            const getRoverUseCase: GetRoverUseCase = new GetRoverUseCase(new RoverFinder(new InMemoryRoverRepository()));
            const rover = await getRoverUseCase.run(this.roverId);
            return rover;
        } catch (error) {
            let message = 'Unknown Error';
            if (error instanceof Error) message = error.message;
            console.error(message);
        }
    }

    static async updatePosition(position: Array<number>) {
        try {
            const roverUpdateDto = RoverUpdateDTO.create(this.roverId, position[0], position[1]);
            const updateRoverUseCase: UpdateRoverUseCase = new UpdateRoverUseCase(new RoverSave(new InMemoryRoverRepository()));
            const rover = await updateRoverUseCase.run(roverUpdateDto);
            return rover;
        } catch (error) {
            let message = 'Unknown Error';
            if (error instanceof Error) message = error.message;
            console.error(message);
        }
    }

    static async collapseDetection(position: Array<number>) {
        try {
            const detectObstaclesUseCase: DetectObstaclesUseCase = new DetectObstaclesUseCase(new CollapseSearcher(new InMemoryRoverRepository()));
            const flag = await detectObstaclesUseCase.run([position[0], position[1]]);

            if (flag) {
                Swal.fire({
                    title: 'Ups!',
                    text: 'El Rover se ha parado porque iba a colisionar',
                    icon: 'error',
                    confirmButtonText: 'Continuar'
                })
            }

            return flag;
        } catch (error) {
            let message = 'Unknown Error';
            if (error instanceof Error) message = error.message;
            console.error(message);
        }
    }

    static calculateMovement(nextMovement: string, oldPosition: Array<number>, direction: string): Array<number> | void {
        if (nextMovement === undefined) {
            Swal.fire({
                title: 'Finish',
                text: 'El Rover se ha parado porque a finalizado el recorrido',
                icon: 'success',
                confirmButtonText: 'Restart'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.reload();
                }
            });
            return;
        }
        const result = new Array<number>();
        const traductionNextMovement: string = this.traductionMovement(nextMovement, direction);
        switch (traductionNextMovement) {
            case 'up':
                result.push(oldPosition[0]);
                result.push(oldPosition[1] - (window.innerHeight / CASILLA_SCALE));
                break;
            case 'left':
                result.push(oldPosition[0] - (window.innerWidth / CASILLA_SCALE));
                result.push(oldPosition[1]);
                break;
            case 'rigth':
                result.push(oldPosition[0] + (window.innerWidth / CASILLA_SCALE));
                result.push(oldPosition[1]);
                break;
            case 'down':
                result.push(oldPosition[0]);
                result.push(oldPosition[1] + (window.innerHeight / CASILLA_SCALE));
                break;
            default:
                result.push(oldPosition[0]);
                result.push(oldPosition[1]);
                break;
        }
        return result;
    }

    static traductionMovement(nextMovement: string, direction: string): string {
        let result = '';
        const finalDirection: string = direction.trim().toLowerCase();
        const finalNextMovement: string = nextMovement.trim().toLowerCase();
        if (finalDirection === "north") {
            if (finalNextMovement === "f") { result = 'up' };
            if (finalNextMovement === 'l') { result = 'left' };
            if (finalNextMovement === 'r') { result = 'right' };
        }

        if (finalDirection === 'east') {
            if (finalNextMovement === 'f') result = 'right';
            if (finalNextMovement === 'l') result = 'up';
            if (finalNextMovement === 'r') result = 'down';
        }

        if (finalDirection === 'west') {
            if (finalNextMovement === 'f') result = 'left';
            if (finalNextMovement === 'l') result = 'down';
            if (finalNextMovement === 'r') result = 'up';
        }

        if (finalDirection === 'south') {
            if (finalNextMovement === 'f') result = 'down';
            if (finalNextMovement === 'l') result = 'right';
            if (finalNextMovement === 'r') result = 'left';
        }

        return result;
    }

}