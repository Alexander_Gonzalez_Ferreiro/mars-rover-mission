import { CollectionObstacles } from "../../domain/CollectionObstacles"
import { Rover } from "../../domain/Rover"
import { CanvasAction, CanvasRoverState, ICanvas } from "../type"
import * as actionTypes from "./actionTypes"

export const reducer = (
    state: CanvasRoverState = { canvas: { rover: Rover.createEmpty(), obstacles: CollectionObstacles.createEmpty(), collapse: false } },
    action: CanvasAction
): CanvasRoverState => {
    switch (action.type) {
        case actionTypes.CREATE_CANVAS:
            const newCanvas: ICanvas = {
                rover: action.canvas.rover,
                obstacles: action.canvas.obstacles,
                collapse: action.canvas.collapse
            }
            return {
                ...state,
                canvas: newCanvas
            }
        case actionTypes.SAVE_ROVER:
            const canvas: ICanvas = {
                rover: action.canvas.rover,
                obstacles: state.canvas.obstacles,
                collapse: state.canvas.collapse
            }
            return {
                ...state,
                canvas
            }

    }
    return state
}
