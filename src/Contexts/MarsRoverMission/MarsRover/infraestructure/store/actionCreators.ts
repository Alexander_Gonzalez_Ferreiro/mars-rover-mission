import { CollectionObstacles } from "../../domain/CollectionObstacles";
import { Rover } from "../../domain/Rover";
import { ObstacleService } from "../services/ObstaclesService";
import { RoverService } from "../services/RoverService";
import { CanvasAction } from "../type";
import * as actionTypes from "./actionTypes"

export function createCanvas(dataMars: any) {
    return async function (dispatch: any) {
        await RoverService.create(dataMars, window.innerWidth / 2, window.innerHeight / 2);
        await ObstacleService.create(1000);

        let obstacles = await ObstacleService.getAll();

        if (obstacles === undefined) {
            obstacles = CollectionObstacles.createEmpty();
        }

        let rover: Rover | undefined = await RoverService.getRover();

        if (rover === undefined) {
            rover = Rover.createEmpty();
        }

        const action: CanvasAction = {
            type: actionTypes.CREATE_CANVAS,
            canvas: { rover, obstacles, collapse: false },
        }
        dispatch(action)
    }
}


export function saveCanvas(position: Array<number>) {
    return async function (dispatch: any) {
        let auxFlag: boolean = true;
        let flag = await RoverService.collapseDetection(position);
        if (flag === false && flag !== undefined) {
            await RoverService.updatePosition(position);
            auxFlag = false;
        }


        let rover: Rover | undefined = await RoverService.getRover();

        if (rover === undefined) {
            rover = Rover.createEmpty();
        }

        let obstacles = await ObstacleService.getAll();

        if (obstacles === undefined) {
            obstacles = CollectionObstacles.createEmpty();
        }

        if (typeof flag === undefined) flag = false;

        const action: CanvasAction = {
            type: actionTypes.SAVE_ROVER,
            canvas: { rover, obstacles, collapse: auxFlag },

        }
        dispatch(action)
    }
}