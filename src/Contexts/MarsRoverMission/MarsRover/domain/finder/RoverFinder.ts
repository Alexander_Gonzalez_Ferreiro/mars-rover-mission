import { RoverIdNotExistsException } from "../Exceptions/RoverIdNotExistsException";
import { Rover } from "../Rover";
import { RoverRepository } from "../RoverRepository";
import { RoverId } from "../ValueObjects/RoverId";

export class RoverFinder {

    constructor(readonly repository: RoverRepository) {

    }

    async execute(roverId: RoverId): Promise<Rover> {
        await this.roverExists(roverId);

        return await this.repository.findRover(roverId);
    }

    private async roverExists(roverId: RoverId): Promise<void> {
        if (!await this.repository.roverExists(roverId)) {
            throw new RoverIdNotExistsException(roverId.value);
        }
    }
}