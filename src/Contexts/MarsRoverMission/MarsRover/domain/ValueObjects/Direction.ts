import { StringValueObject } from "../../../../Shared/domain/ValueObjects/StringValueObject";
import { InvalidDirectionException } from "../Exceptions/InvalidDirectionException";

export class Direction extends StringValueObject {
    private validDirections: Array<String> = ['North', 'East', 'West', 'South'];

    constructor(value: string) {
        super(value);
        this.isValidDirection(value);
    }

    isValidDirection(value: string) {
        if (!this.validDirections.includes(value)) {
            throw new InvalidDirectionException(value);
        }
    }
}