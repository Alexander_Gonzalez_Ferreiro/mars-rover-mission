import { NumberValueObject } from "../../../../Shared/domain/ValueObjects/NumberValueObject";

export class Position extends NumberValueObject {
    constructor(value: number) {
        super(value);
    }
}