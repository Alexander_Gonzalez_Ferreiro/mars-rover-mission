import { NumberValueObject } from "../../../../Shared/domain/ValueObjects/NumberValueObject";

export class RoverId extends NumberValueObject {
    constructor(value: number) {
        super(value);
    }
}