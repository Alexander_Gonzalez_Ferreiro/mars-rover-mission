import { Obstacle } from "./Obstacle";
export class CollectionObstacles {
    readonly obstacles: Array<Obstacle>;

    private constructor({ obstacles }: { obstacles: Array<Obstacle> }) {
        this.obstacles = obstacles;
    }

    public static create(obstacles: Array<Obstacle>): CollectionObstacles {
        return new CollectionObstacles({ obstacles });
    }

    public static createEmpty(): CollectionObstacles {
        return new CollectionObstacles({ obstacles: new Array<Obstacle>() });
    }

    public addObstacle(obstacle: Obstacle): void {
        this.obstacles.push(obstacle);
    }
}