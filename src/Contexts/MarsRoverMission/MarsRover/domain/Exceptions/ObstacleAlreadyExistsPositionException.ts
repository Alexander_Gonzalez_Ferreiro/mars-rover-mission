export class ObstacleAlreadyExistsPositionException extends Error {
    constructor() {
        super(`In this position already exists an obstacle.`);
    }
}