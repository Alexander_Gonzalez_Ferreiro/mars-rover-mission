export class InvalidDirectionException extends Error {
    constructor(name: string) {
        super(`This direction <${name}> is invalid. Please select one of this: North, East, West or South.`);
    }
}