export class RoverIdNotExistsException extends Error {
    constructor(id: number) {
        super(`Rover with id <${id}> not exists.`);
    }
}