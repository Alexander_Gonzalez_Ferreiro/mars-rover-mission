export class RoverIdAlreadyExistsException extends Error {
    constructor(id: number) {
        super(`Rover id <${id}> already exists.`);
    }
}