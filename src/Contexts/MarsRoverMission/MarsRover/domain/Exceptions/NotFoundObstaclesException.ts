export class NotFoundObstaclesException extends Error {
    constructor() {
        super(`Obstacles not found.`);
    }
}