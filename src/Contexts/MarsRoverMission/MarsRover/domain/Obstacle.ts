import { Coordinates } from "./Coordinates";

export class Obstacle {
    readonly coordinates: Coordinates;

    private constructor({ coordinates }: { coordinates: Coordinates }) {
        this.coordinates = coordinates;
    }

    public static create(coordinates: Coordinates): Obstacle {
        return new Obstacle({ coordinates });
    }
}