import { Coordinates } from "../Coordinates";
import { RoverIdAlreadyExistsException } from "../Exceptions/RoverIdAlreadyExistsException";
import { RoverRepository } from "../RoverRepository";
import { Direction } from "../ValueObjects/Direction";
import { RoverId } from "../ValueObjects/RoverId";

export class RoverCreator {
    constructor(readonly repository: RoverRepository) {

    }

    async execute(roverId: RoverId, coordinates: Coordinates, direction: Direction): Promise<void> {
        await this.hasDuplicatesGuard(roverId);

        return await this.repository.createRover(roverId, coordinates, direction);
    }

    private async hasDuplicatesGuard(roverId: RoverId): Promise<void> {
        if (await this.repository.roverExists(roverId)) {
            throw new RoverIdAlreadyExistsException(roverId.value);
        }
    }
}