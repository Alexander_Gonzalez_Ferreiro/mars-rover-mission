import { Coordinates } from "../Coordinates";
import { ObstacleAlreadyExistsPositionException } from "../Exceptions/ObstacleAlreadyExistsPositionException";
import { RoverRepository } from "../RoverRepository";

export class ObstacleCreator {
    constructor(readonly repository: RoverRepository) {

    }

    async execute(coordinates: Coordinates): Promise<void> {
        await this.hasDuplicatesGuard(coordinates);
        return await this.repository.createObstacle(coordinates);
    }

    private async hasDuplicatesGuard(coordinates: Coordinates): Promise<void> {
        if (await this.repository.existObstacle(coordinates)) {
            throw new ObstacleAlreadyExistsPositionException();
        }
    }
}