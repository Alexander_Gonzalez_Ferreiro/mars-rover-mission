import { CollectionObstacles } from "./CollectionObstacles";
import { Coordinates } from "./Coordinates";
import { Obstacle } from "./Obstacle";
import { Rover } from "./Rover";
import { Direction } from "./ValueObjects/Direction";
import { RoverId } from "./ValueObjects/RoverId";

export interface RoverRepository {
    createRover(roverId: RoverId, coordinates: Coordinates, direction: Direction): Promise<void>;
    findRover(roverId: RoverId): Promise<Rover>;
    roverExists(roverId: RoverId): Promise<boolean>;
    createObstacle(coordinates: Coordinates): Promise<void>;
    searchObstacles(): Promise<CollectionObstacles>;
    existsObstacles(): Promise<Boolean>;
    saveRover(roverId: RoverId, coordinates: Coordinates): Promise<void>;
    existObstacle(coordinates: Coordinates): Promise<Boolean>;
}