import { CollectionObstacles } from "../CollectionObstacles";
import { NotFoundObstaclesException } from "../Exceptions/NotFoundObstaclesException";
import { RoverRepository } from "../RoverRepository";

export class ObstacleSearcher {

    constructor(readonly repository: RoverRepository) {

    }

    async execute(): Promise<CollectionObstacles> {
        this.hasObstaclesGuard();
        return await this.repository.searchObstacles();
    }

    private async hasObstaclesGuard(): Promise<void> {
        if (!await this.repository.existsObstacles()) {
            throw new NotFoundObstaclesException();
        }
    }
}