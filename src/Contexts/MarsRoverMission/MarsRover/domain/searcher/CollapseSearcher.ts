import { Coordinates } from "../Coordinates";
import { RoverRepository } from "../RoverRepository";
import { Position } from "../ValueObjects/Position";

export class CollapseSearcher {

    constructor(readonly repository: RoverRepository) {

    }

    async execute(position: Position[]): Promise<Boolean> {
        const coordinates = Coordinates.create(position[0], position[1]);
        return await this.repository.existObstacle(coordinates);
    }
}