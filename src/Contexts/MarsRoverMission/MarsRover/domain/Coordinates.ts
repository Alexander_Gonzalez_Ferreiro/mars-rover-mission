import { Position } from "./ValueObjects/Position";

export class Coordinates {
    readonly positionX: Position;
    readonly positionY: Position;

    private constructor({ positionX, positionY }: { positionX: Position; positionY: Position }) {
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public static create(positionX: Position, positionY: Position): Coordinates {
        return new Coordinates({ positionX, positionY });
    }
}