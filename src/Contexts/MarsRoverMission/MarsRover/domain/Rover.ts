import { Coordinates } from "./Coordinates";
import { Direction } from "./ValueObjects/Direction";
import { Position } from "./ValueObjects/Position";
import { RoverId } from "./ValueObjects/RoverId";

export class Rover {
    readonly id: RoverId;
    coordinates: Coordinates;
    readonly direction: Direction;

    private constructor({ id, coordinates, direction }: { id: RoverId; coordinates: Coordinates; direction: Direction }) {
        this.id = id;
        this.coordinates = coordinates;
        this.direction = direction;
    }

    public static create(id: RoverId, coordinates: Coordinates, direction: Direction): Rover {
        return new Rover({ id, coordinates, direction });
    }

    public static createEmpty(): Rover {
        const id: RoverId = new RoverId(0);
        const coordinates: Coordinates = Coordinates.create(new Position(0), new Position(0));
        const direction: Direction = new Direction('North');
        return new Rover({ id, coordinates, direction });
    }

    public updateCoordinates(coordinates: Coordinates) {
        this.coordinates = coordinates;
    }
}