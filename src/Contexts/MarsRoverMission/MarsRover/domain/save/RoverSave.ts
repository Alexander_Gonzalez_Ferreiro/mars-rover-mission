import { Coordinates } from "../Coordinates";
import { RoverIdNotExistsException } from "../Exceptions/RoverIdNotExistsException";
import { RoverRepository } from "../RoverRepository";
import { RoverId } from "../ValueObjects/RoverId";

export class RoverSave {
    constructor(readonly repository: RoverRepository) {

    }

    async execute(roverId: RoverId, coordinates: Coordinates): Promise<void> {
        await this.existsGuard(roverId);

        return await this.repository.saveRover(roverId, coordinates);
    }

    private async existsGuard(roverId: RoverId): Promise<void> {
        if (!await this.repository.roverExists(roverId)) {
            throw new RoverIdNotExistsException(roverId.value);
        }
    }
}