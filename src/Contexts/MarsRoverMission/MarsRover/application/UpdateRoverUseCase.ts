import { Coordinates } from "../domain/Coordinates";
import { RoverSave } from "../domain/save/RoverSave";
import { Position } from "../domain/ValueObjects/Position";
import { RoverId } from "../domain/ValueObjects/RoverId";
import { RoverUpdateDTO } from "./DTO/RoverUpdateDTO";

export class UpdateRoverUseCase {

    constructor(private repository: RoverSave) {

    }

    async run(roverUpdateDTO: RoverUpdateDTO): Promise<void> {
        const roverId: RoverId = new RoverId(roverUpdateDTO.roverId);
        const positionX: Position = new Position(roverUpdateDTO.positionX);
        const positionY: Position = new Position(roverUpdateDTO.positionY);
        const coordinates: Coordinates = Coordinates.create(positionX, positionY);
        await this.repository.execute(roverId, coordinates);
    }
}