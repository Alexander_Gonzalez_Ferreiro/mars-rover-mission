export class RoverUpdateDTO {
    readonly roverId: number;
    readonly positionX: number;
    readonly positionY: number;

    private constructor({ roverId, positionX, positionY }: { roverId: number; positionX: number; positionY: number }) {
        this.roverId = roverId;
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public static create(roverId: number, positionX: number, positionY: number): RoverUpdateDTO {
        return new RoverUpdateDTO({ roverId, positionX, positionY });
    }
}