export class RoverDTO {
    readonly roverId: number;
    readonly positionX: number;
    readonly positionY: number;
    readonly direction: string;

    private constructor({ roverId, positionX, positionY, direction }: { roverId: number; positionX: number; positionY: number; direction: string }) {
        this.roverId = roverId;
        this.positionX = positionX;
        this.positionY = positionY;
        this.direction = direction;
    }

    public static create(roverId: number, positionX: number, positionY: number, direction: string): RoverDTO {
        return new RoverDTO({ roverId, positionX, positionY, direction });
    }
}