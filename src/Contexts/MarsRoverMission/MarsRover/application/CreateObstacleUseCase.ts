import { Coordinates } from '../domain/Coordinates';
import { ObstacleCreator } from '../domain/create/ObstacleCreator';
import { Position } from '../domain/ValueObjects/Position';

export class CreateObstacleUseCase {

    constructor(private repository: ObstacleCreator) {

    }

    async run(cuantity: number): Promise<void> {
        for (let index = 0; index < cuantity; index++) {
            let positionX = new Position(Math.random() * (window.innerWidth - 1) + 1);
            let positionY = new Position(Math.random() * (window.innerHeight - 1) + 1);
            try {
                await this.repository.execute(Coordinates.create(positionX, positionY));
            } catch (err) {
                continue;
            }
        }
    }
}