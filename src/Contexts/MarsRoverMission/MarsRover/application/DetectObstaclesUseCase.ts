import { CollapseSearcher } from "../domain/searcher/CollapseSearcher";
import { Position } from "../domain/ValueObjects/Position";

export class DetectObstaclesUseCase {

    constructor(private repository: CollapseSearcher) {

    }

    async run(position: Array<number>): Promise<Boolean> {
        const positionX = new Position(position[0]);
        const positionY = new Position(position[1]);
        return await this.repository.execute([positionX, positionY]);
    }
}