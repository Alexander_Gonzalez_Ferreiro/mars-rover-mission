import { RoverFinder } from "../domain/finder/RoverFinder";
import { Rover } from "../domain/Rover";
import { RoverId } from "../domain/ValueObjects/RoverId";

export class GetRoverUseCase {

    constructor(private repository: RoverFinder) {

    }

    async run(id: number): Promise<Rover> {
        const roverId: RoverId = new RoverId(id);
        return await this.repository.execute(roverId);
    }
}