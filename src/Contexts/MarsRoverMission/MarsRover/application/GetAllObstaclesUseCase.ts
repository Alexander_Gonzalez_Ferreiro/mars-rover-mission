
import { CollectionObstacles } from "../domain/CollectionObstacles";
import { ObstacleSearcher } from "../domain/searcher/ObstacleSearcher";

export class GetAllObstaclesUseCase {

    constructor(private repository: ObstacleSearcher) {

    }

    async run(): Promise<CollectionObstacles> {
        return await this.repository.execute();
    }
}