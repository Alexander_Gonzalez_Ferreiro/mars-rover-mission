import { Coordinates } from '../domain/Coordinates';
import { RoverCreator } from '../domain/create/RoverCreator';
import { Direction } from '../domain/ValueObjects/Direction';
import { Position } from '../domain/ValueObjects/Position';
import { RoverId } from '../domain/ValueObjects/RoverId';
import { RoverDTO } from './DTO/RoverDTO';

export class CreateRoverUseCase {

    constructor(private repository: RoverCreator) {

    }

    async run(roverDTO: RoverDTO): Promise<void> {
        const roverId: RoverId = new RoverId(roverDTO.roverId);
        const positionX: Position = new Position(roverDTO.positionX);
        const positionY: Position = new Position(roverDTO.positionY);
        const coordinates: Coordinates = Coordinates.create(positionX, positionY);
        const direction: Direction = new Direction(roverDTO.direction);
        await this.repository.execute(roverId, coordinates, direction);
    }
}