import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Contexts/MarsRoverMission/MarsRover/infraestructure/components/App';
import reportWebVitals from './reportWebVitals';
import { createStore, applyMiddleware, Store } from "redux"
import { Provider } from "react-redux"
import { reducer } from "./Contexts/MarsRoverMission/MarsRover/infraestructure/store/reducer";
import thunk from "redux-thunk"
import { CanvasAction, CanvasRoverState, DispatchType } from './Contexts/MarsRoverMission/MarsRover/infraestructure/type';


const store: Store<CanvasRoverState, CanvasAction> & {
  dispatch: DispatchType
} = createStore(reducer, applyMiddleware(thunk))
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
