import { DetectObstaclesUseCase } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/application/DetectObstaclesUseCase";
import { Coordinates } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Coordinates";
import { ObstacleCreator } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/ObstacleCreator";
import { CollapseSearcher } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/searcher/CollapseSearcher";
import { PositionMother } from "../domain/ValueObjects/PositionMother";
import { InMemoryRoverRepositoryMock } from "../__mocks__/InMemoryRoverRepositoryMock";

let repository: InMemoryRoverRepositoryMock;
let searcher: CollapseSearcher;
let useCase: DetectObstaclesUseCase;
let creator: ObstacleCreator;

beforeEach(async () => {
    repository = new InMemoryRoverRepositoryMock();
    searcher = new CollapseSearcher(repository);
    useCase = new DetectObstaclesUseCase(searcher);
});

describe('Detect obstacles should find something', () => {
    it('should return true', async () => {
        const positionX = PositionMother.generate();
        const positionY = PositionMother.generate();
        await new ObstacleCreator(repository).execute(Coordinates.create(positionX, positionY)).catch(err => { throw err });

        const result = await useCase.run([positionX.value, positionY.value]).catch(err => { throw err });

        expect(result).toBeTruthy();
    });

    it('should return false', async () => {
        const positionX = PositionMother.generate();
        const positionY = PositionMother.generate();
        const result = await useCase.run([positionX.value, positionY.value]).catch(err => { throw err });

        expect(result).toBeFalsy();
    });
});