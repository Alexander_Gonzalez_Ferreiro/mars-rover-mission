import 'reflect-metadata'; // Import only once
import { RoverCreator } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/RoverCreator";
import { InMemoryRoverRepositoryMock } from "../__mocks__/InMemoryRoverRepositoryMock";
import { CreateRoverUseCase } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/application/CreateRoverUseCase";
import { RoverIdAlreadyExistsException } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Exceptions/RoverIdAlreadyExistsException";
import { RoverDTO } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/application/DTO/RoverDTO";
import { RoverDTOMother } from "./DTO/RoverDTOMother";
import { Rover } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Rover";
import { RoverId } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/RoverId";
import { Coordinates } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Coordinates";
import { Direction } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/Direction";
import { Position } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/Position";

let repository: InMemoryRoverRepositoryMock;
let creator: RoverCreator;
let useCase: CreateRoverUseCase;

beforeEach(() => {
    repository = new InMemoryRoverRepositoryMock();
    creator = new RoverCreator(repository);
    useCase = new CreateRoverUseCase(creator);
});

describe('Creator rover UseCase', () => {
    it('should return a valid Rover', async () => {
        const roverDto: RoverDTO = RoverDTOMother.create();

        await useCase.run(roverDto).catch(err => { throw err });

        const coordinates: Coordinates = Coordinates.create(new Position(roverDto.positionX), new Position(roverDto.positionY));
        const rover: Rover = Rover.create(new RoverId(roverDto.roverId), coordinates, new Direction(roverDto.direction));

        repository.assertRover(rover);
    });

    it('should return an error with repeat Rover id', async () => {
        const test = async () => {
            const roverDto: RoverDTO = RoverDTOMother.create();

            await useCase.run(roverDto).catch(err => { throw err });

            await useCase.run(roverDto).catch(err => { throw err });
        };
        await expect(test())
            .rejects
            .toThrow(RoverIdAlreadyExistsException);
    });
});
