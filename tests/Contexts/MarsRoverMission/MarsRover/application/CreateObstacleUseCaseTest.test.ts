import { ObstacleCreator } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/ObstacleCreator";
import { InMemoryRoverRepositoryMock } from "../__mocks__/InMemoryRoverRepositoryMock";
import { CreateObstacleUseCase } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/application/CreateObstacleUseCase";

let repository: InMemoryRoverRepositoryMock;
let creator: ObstacleCreator;
let useCase: CreateObstacleUseCase;

beforeEach(() => {
    repository = new InMemoryRoverRepositoryMock();
    creator = new ObstacleCreator(repository);
    useCase = new CreateObstacleUseCase(creator);
});

describe('Creator obstacle UseCase', () => {
    it('should validate errors', async () => {
        const test = async () => {
            const cuantity: number = Math.floor(Math.random() * 10);

            await useCase.run(cuantity).catch(err => { throw err });
        };
        await expect(test())
            .rejects
            .toThrow(ReferenceError);
    });
});
