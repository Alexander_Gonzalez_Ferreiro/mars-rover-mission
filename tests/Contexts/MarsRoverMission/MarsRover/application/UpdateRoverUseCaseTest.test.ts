import { RoverCreator } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/RoverCreator";
import { RoverIdNotExistsException } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Exceptions/RoverIdNotExistsException";
import { RoverId } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/RoverId";
import { CoordinatesMother } from "../domain/CoordinatesMother";
import { DirectionMother } from "../domain/ValueObjects/DirectionMother";
import { RoverIdMother } from "../domain/ValueObjects/RoverIdMother";
import { InMemoryRoverRepositoryMock } from "../__mocks__/InMemoryRoverRepositoryMock";
import { UpdateRoverUseCase } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/application/UpdateRoverUseCase";
import { RoverSave } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/save/RoverSave";
import { RoverUpdateDTO } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/application/DTO/RoverUpdateDTO";
import { PositionMother } from "../domain/ValueObjects/PositionMother";
import { Rover } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Rover";
import { Coordinates } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Coordinates";

let repository: InMemoryRoverRepositoryMock;
let saver: RoverSave;
let roverId: RoverId;
let useCase: UpdateRoverUseCase;

beforeEach(async () => {
    repository = new InMemoryRoverRepositoryMock();
    saver = new RoverSave(repository);
    useCase = new UpdateRoverUseCase(saver);
    roverId = RoverIdMother.generate();
    await new RoverCreator(repository).execute(roverId, CoordinatesMother.create(), DirectionMother.generate());
});

describe('Saver should save rover use case', () => {
    it('should save a valid Rover', async () => {
        const positionX = PositionMother.generate();
        const positionY = PositionMother.generate();
        const roverUpdateDTO = RoverUpdateDTO.create(roverId.value, positionX.value, positionY.value);
        await useCase.run(roverUpdateDTO);
        const rover = Rover.create(roverId, Coordinates.create(positionX, positionY), DirectionMother.generate());

        repository.assertRover(rover);
    });

    it('should return an error of non Rover id found', async () => {
        const test = async () => {
            const id = new RoverId(99);

            await useCase.run(RoverUpdateDTO.create(id.value, PositionMother.generate().value, PositionMother.generate().value));
        };
        await expect(test())
            .rejects
            .toThrow(RoverIdNotExistsException);
    });
});