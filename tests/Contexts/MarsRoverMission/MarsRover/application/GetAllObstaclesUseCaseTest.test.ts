import { GetAllObstaclesUseCase } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/application/GetAllObstaclesUseCase";
import { ObstacleCreator } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/ObstacleCreator";
import { ObstacleSearcher } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/searcher/ObstacleSearcher";
import { CoordinatesMother } from "../domain/CoordinatesMother";
import { InMemoryRoverRepositoryMock } from "../__mocks__/InMemoryRoverRepositoryMock";

let repository: InMemoryRoverRepositoryMock;
let searcher: ObstacleSearcher;
let useCase: GetAllObstaclesUseCase;

beforeEach(async () => {
    repository = new InMemoryRoverRepositoryMock();
    searcher = new ObstacleSearcher(repository);
    useCase = new GetAllObstaclesUseCase(searcher);
    await new ObstacleCreator(repository).execute(CoordinatesMother.create()).catch(err => { throw err });
});

describe('Get obstacles UseCase', () => {
    it('should validate objects createds', async () => {
        const obstacles = await useCase.run().catch(err => { throw err });

        expect(obstacles.obstacles.length).toBeGreaterThanOrEqual(1);
    });
});
