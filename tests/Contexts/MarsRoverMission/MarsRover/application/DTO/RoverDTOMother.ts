import { RoverDTO } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/application/DTO/RoverDTO";

export class RoverDTOMother {
    public static create(): RoverDTO {
        let randomValue = ['North', 'East', 'West', 'South'];
        let randomIndex = Math.floor(Math.random() * (4 - 0)) + 0;
        return RoverDTO.create(Math.floor(Math.random() * 10), Math.floor(Math.random() * 10), Math.floor(Math.random() * 10), randomValue[randomIndex]);
    }
}