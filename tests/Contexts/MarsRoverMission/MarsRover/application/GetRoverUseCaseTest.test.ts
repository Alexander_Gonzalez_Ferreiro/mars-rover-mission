import 'reflect-metadata'; // Import only once
import { RoverFinder } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/finder/RoverFinder";
import { InMemoryRoverRepositoryMock } from "../__mocks__/InMemoryRoverRepositoryMock";
import { GetRoverUseCase } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/application/GetRoverUseCase";
import { RoverId } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/RoverId";
import { RoverIdMother } from "../domain/ValueObjects/RoverIdMother";
import { RoverCreator } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/RoverCreator";
import { CoordinatesMother } from "../domain/CoordinatesMother";
import { DirectionMother } from "../domain/ValueObjects/DirectionMother";
import { RoverIdNotExistsException } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Exceptions/RoverIdNotExistsException";

let repository: InMemoryRoverRepositoryMock;
let finder: RoverFinder;
let useCase: GetRoverUseCase;
let roverId: RoverId

beforeEach(async () => {
    repository = new InMemoryRoverRepositoryMock();
    finder = new RoverFinder(repository);
    useCase = new GetRoverUseCase(finder);
    roverId = RoverIdMother.generate();
    await new RoverCreator(repository).execute(roverId, CoordinatesMother.create(), DirectionMother.generate());
});

describe('Finder should find rover', () => {
    it('should return a valid Rover', async () => {
        const rover = await useCase.run(roverId.value).catch(err => { throw err });

        repository.assertRover(rover);
    });

    it('should return an error of non Rover id found', async () => {
        const test = async () => {
            const id = new RoverId(99);

            await useCase.run(id.value).catch(err => { throw err });
        };
        await expect(test())
            .rejects
            .toThrow(RoverIdNotExistsException);
    });
});