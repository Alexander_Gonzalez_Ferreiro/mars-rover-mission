import { CollectionObstacles } from '../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/CollectionObstacles';
import { Coordinates } from '../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Coordinates';
import { Obstacle } from '../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Obstacle';
import { Rover } from '../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Rover';
import { RoverRepository } from '../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/RoverRepository';
import { Direction } from '../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/Direction';
import { RoverId } from '../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/RoverId';
import { RoverMother } from '../domain/RoverMother';

export class InMemoryRoverRepositoryMock implements RoverRepository {
    private mockSave = jest.fn();

    async createRover(roverId: RoverId, coordinates: Coordinates, direction: Direction): Promise<void> {
        const rover: Rover = Rover.create(roverId, coordinates, direction);
        this.mockSave(rover);
    };

    async findRover(roverId: RoverId): Promise<Rover> {
        const mock = this.mockSave.mock;
        const lastSavedRover = mock.calls[mock.calls.length - 1][0] as Rover;
        return lastSavedRover;
    };

    async roverExists(roverId: RoverId): Promise<boolean> {
        const mock = this.mockSave.mock;
        if (mock.calls[mock.calls.length - 1] != undefined) {
            const lastSavedRover = mock.calls[mock.calls.length - 1][0] as Rover;
            if (lastSavedRover.id.value == roverId.value) {
                return true;
            }

        }

        return false;
    }

    async createObstacle(coordinates: Coordinates): Promise<void> {
        const obstacle: Obstacle = Obstacle.create(coordinates);
        this.mockSave(obstacle);
    };

    async searchObstacles(): Promise<CollectionObstacles> {
        const mock = this.mockSave.mock;
        const lastCollectionObstacles = CollectionObstacles.create([mock.calls[mock.calls.length - 1][0] as Obstacle]);
        return lastCollectionObstacles;
    };

    async existsObstacles(): Promise<Boolean> {
        const mock = this.mockSave.mock;
        if (mock.calls[mock.calls.length - 1] != undefined) {
            return true;
        }

        return false;
    };

    async saveRover(roverId: RoverId, coordinates: Coordinates): Promise<void> {
        const mock = this.mockSave.mock;
        const lastSavedRover: Rover = mock.calls[mock.calls.length - 1][0] as Rover;
        const newRover: Rover = Rover.create(roverId, coordinates, lastSavedRover.direction);
        this.mockSave(newRover);
    };

    async existObstacle(coordinates: Coordinates): Promise<Boolean> {
        const mock = this.mockSave.mock;
        if (mock.calls[mock.calls.length - 1] != undefined) {
            const lastSavedObstacle = mock.calls[mock.calls.length - 1][0] as Obstacle;
            if (lastSavedObstacle.coordinates.positionX.value == coordinates.positionX.value && coordinates.positionY.value == lastSavedObstacle.coordinates.positionY.value) {
                return true;
            }

        }

        return false;
    };

    assertRover(expected: Rover): void {
        const mock = this.mockSave.mock;
        const lastSavedRover = mock.calls[mock.calls.length - 1][0] as Rover;
        expect(lastSavedRover).toBeInstanceOf(Rover);
        expect(lastSavedRover.id.value).toEqual(expected.id.value);
    }

    assertObstacle(expected: Obstacle): void {
        const mock = this.mockSave.mock;
        const lastSavedObstacle = mock.calls[mock.calls.length - 1][0] as Obstacle;
        expect(lastSavedObstacle).toBeInstanceOf(Obstacle);
        expect(lastSavedObstacle.coordinates.positionX.value).toEqual(expected.coordinates.positionX.value);
    }

    assertObstacles(expected: CollectionObstacles): void {
        expect(expected).toBeInstanceOf(CollectionObstacles);
        expect(expected.obstacles.length).toBeGreaterThanOrEqual(1);
    }

    assertNotEmpty() {
        const mock = this.mockSave.mock;
        const result = mock.calls[mock.calls.length - 1].length;
        expect(result).toBeGreaterThanOrEqual(1);
    }
}