import { ObstacleCreator } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/ObstacleCreator";
import { ObstacleSearcher } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/searcher/ObstacleSearcher";
import { InMemoryRoverRepositoryMock } from "../../__mocks__/InMemoryRoverRepositoryMock";
import { CoordinatesMother } from "../CoordinatesMother";

let repository: InMemoryRoverRepositoryMock;
let searcher: ObstacleSearcher;
let creator: ObstacleCreator;

beforeEach(async () => {
    repository = new InMemoryRoverRepositoryMock();
    searcher = new ObstacleSearcher(repository);
    creator = new ObstacleCreator(repository);
    await creator.execute(CoordinatesMother.create()).catch(err => { throw err });
});

describe('Searcher should find something', () => {
    it('should return obstacles', async () => {
        const result = await searcher.execute().catch(err => { throw err });
        repository.assertObstacles(result);
    });
});