import { RoverCreator } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/RoverCreator";
import { RoverId } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/RoverId";
import { InMemoryRoverRepositoryMock } from "../../__mocks__/InMemoryRoverRepositoryMock";
import { CoordinatesMother } from "../CoordinatesMother";
import { DirectionMother } from "../ValueObjects/DirectionMother";
import { RoverIdMother } from "../ValueObjects/RoverIdMother";
import { CollapseSearcher } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/searcher/CollapseSearcher";
import { PositionMother } from "../ValueObjects/PositionMother";
import { ObstacleCreator } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/ObstacleCreator";
import { Coordinates } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Coordinates";

let repository: InMemoryRoverRepositoryMock;
let searcher: CollapseSearcher;
let roverId: RoverId;

beforeEach(async () => {
    repository = new InMemoryRoverRepositoryMock();
    searcher = new CollapseSearcher(repository);
    roverId = RoverIdMother.generate();
});

describe('Searcher should find something', () => {
    it('should return true', async () => {
        const positionX = PositionMother.generate();
        const positionY = PositionMother.generate();
        await new ObstacleCreator(repository).execute(Coordinates.create(positionX, positionY)).catch(err => { throw err });

        const result = await searcher.execute([positionX, positionY]).catch(err => { throw err });

        expect(result).toBeTruthy();
    });

    it('should return false', async () => {
        const result = await searcher.execute([PositionMother.generate()]).catch(err => { throw err });

        expect(result).toBeFalsy();
    });
});