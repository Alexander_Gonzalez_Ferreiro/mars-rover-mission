import { Direction } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/Direction";

export class DirectionMother extends Direction {
    constructor(value: string) {
        super(value);
    }

    public static generate() {
        let randomValue = ['North', 'East', 'West', 'South'];
        let randomIndex = Math.floor(Math.random() * (4 - 0)) + 0;
        return new Direction(randomValue[randomIndex]);
    }
}