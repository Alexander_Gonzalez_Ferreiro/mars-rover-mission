import { RoverId } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/RoverId";

export class RoverIdMother extends RoverId {
    constructor(value: number) {
        super(value);
    }

    public static generate() {
        let value: number = Math.floor(Math.random() * 10);
        return new RoverId(value);
    }
}