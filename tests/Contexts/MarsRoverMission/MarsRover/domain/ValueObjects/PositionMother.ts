import { Position } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/Position";

export class PositionMother extends Position {
    constructor(value: number) {
        super(value);
    }

    public static generate() {
        let value: number = Math.floor(Math.random() * 100);
        return new Position(value);
    }
}