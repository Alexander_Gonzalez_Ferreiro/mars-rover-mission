import { Obstacle } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Obstacle";
import { CoordinatesMother } from "./CoordinatesMother";

export class ObstacleMother {
    public static create(): Obstacle {
        return Obstacle.create(CoordinatesMother.create());
    }
}