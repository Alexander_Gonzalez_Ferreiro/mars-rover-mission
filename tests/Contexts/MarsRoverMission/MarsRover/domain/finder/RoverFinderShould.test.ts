import 'reflect-metadata'; // Import only once
import { InMemoryRoverRepositoryMock } from "../../__mocks__/InMemoryRoverRepositoryMock";
import { RoverFinder } from '../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/finder/RoverFinder';
import { RoverIdNotExistsException } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Exceptions/RoverIdNotExistsException";
import { RoverIdMother } from "../ValueObjects/RoverIdMother";
import { RoverId } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/RoverId";
import { RoverCreator } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/RoverCreator";
import { CoordinatesMother } from "../CoordinatesMother";
import { DirectionMother } from "../ValueObjects/DirectionMother";

let repository: InMemoryRoverRepositoryMock;
let finder: RoverFinder;
let roverId: RoverId;

beforeEach(async () => {
    repository = new InMemoryRoverRepositoryMock();
    finder = new RoverFinder(repository);
    roverId = RoverIdMother.generate();
    await new RoverCreator(repository).execute(roverId, CoordinatesMother.create(), DirectionMother.generate());
});

describe('Finder should find rover', () => {
    it('should return a valid Rover', async () => {
        const rover = await finder.execute(roverId).catch(err => { throw err });

        repository.assertRover(rover);
    });

    it('should return an error of non Rover id found', async () => {
        const test = async () => {
            const id = new RoverId(99);

            await finder.execute(id).catch(err => { throw err });
        };
        await expect(test())
            .rejects
            .toThrow(RoverIdNotExistsException);
    });
});