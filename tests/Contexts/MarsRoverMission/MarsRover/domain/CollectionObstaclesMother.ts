import { CollectionObstacles } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/CollectionObstacles";
import { Obstacle } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Obstacle";
import { CoordinatesMother } from "./CoordinatesMother";

export class CollectionObstacleMother {
    public static create(): CollectionObstacles {
        return CollectionObstacles.create(new Array<Obstacle>(Obstacle.create(CoordinatesMother.create())));
    }
}