import { Rover } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Rover";
import { CoordinatesMother } from "./CoordinatesMother";
import { DirectionMother } from "./ValueObjects/DirectionMother";
import { RoverIdMother } from "./ValueObjects/RoverIdMother";

export class RoverMother {
    public static create(): Rover {
        return Rover.create(RoverIdMother.generate(), CoordinatesMother.create(), DirectionMother.generate());
    }
}