import { RoverCreator } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/RoverCreator";
import { RoverId } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/ValueObjects/RoverId";
import { InMemoryRoverRepositoryMock } from "../../__mocks__/InMemoryRoverRepositoryMock";
import { CoordinatesMother } from "../CoordinatesMother";
import { DirectionMother } from "../ValueObjects/DirectionMother";
import { RoverIdMother } from "../ValueObjects/RoverIdMother";
import { RoverSave } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/save/RoverSave";
import { RoverIdNotExistsException } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Exceptions/RoverIdNotExistsException";
import { Rover } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Rover";

let repository: InMemoryRoverRepositoryMock;
let saver: RoverSave;
let roverId: RoverId;

beforeEach(async () => {
    repository = new InMemoryRoverRepositoryMock();
    saver = new RoverSave(repository);
    roverId = RoverIdMother.generate();
    await new RoverCreator(repository).execute(roverId, CoordinatesMother.create(), DirectionMother.generate());
});

describe('Saver should save rover', () => {
    it('should save a valid Rover', async () => {
        await saver.execute(roverId, CoordinatesMother.create()).catch(err => { throw err });
        const rover = Rover.create(roverId, CoordinatesMother.create(), DirectionMother.generate());
        repository.assertRover(rover);
    });

    it('should return an error of non Rover id found', async () => {
        const test = async () => {
            const id = new RoverId(99);

            await saver.execute(id, CoordinatesMother.create()).catch(err => { throw err });
        };
        await expect(test())
            .rejects
            .toThrow(RoverIdNotExistsException);
    });
});