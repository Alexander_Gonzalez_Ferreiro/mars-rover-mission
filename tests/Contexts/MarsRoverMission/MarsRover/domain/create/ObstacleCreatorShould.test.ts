import { InMemoryRoverRepositoryMock } from "../../__mocks__/InMemoryRoverRepositoryMock";
import { ObstacleCreator } from '../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/ObstacleCreator';
import { ObstacleAlreadyExistsPositionException } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Exceptions/ObstacleAlreadyExistsPositionException";
import { CoordinatesMother } from "../CoordinatesMother";
import { Obstacle } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Obstacle";

let repository: InMemoryRoverRepositoryMock;
let creator: ObstacleCreator;
beforeEach(() => {
    repository = new InMemoryRoverRepositoryMock();
    creator = new ObstacleCreator(repository);
});

describe('Creator should create obstacle', () => {
    it('should create a valid Obstacle', async () => {

        const coordinates = CoordinatesMother.create();
        await creator.execute(coordinates).catch(err => { console.log(err) });

        const obstacle: Obstacle = Obstacle.create(coordinates);

        repository.assertObstacle(obstacle);
    });

    it('should return an error of repeat obstacle', async () => {
        const test = async () => {
            const coordinates = CoordinatesMother.create();
            await creator.execute(coordinates).catch(err => { throw err });

            await creator.execute(coordinates).catch(err => { throw err });
        };
        await expect(test())
            .rejects
            .toThrow(ObstacleAlreadyExistsPositionException);
    });

});