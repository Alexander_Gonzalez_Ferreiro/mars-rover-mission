import { InMemoryRoverRepositoryMock } from "../../__mocks__/InMemoryRoverRepositoryMock";
import { RoverCreator } from '../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/create/RoverCreator';
import { RoverIdMother } from "../ValueObjects/RoverIdMother";
import { CoordinatesMother } from "../CoordinatesMother";
import { DirectionMother } from "../ValueObjects/DirectionMother";
import { Rover } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Rover";
import { RoverIdAlreadyExistsException } from "../../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Exceptions/RoverIdAlreadyExistsException";

let repository: InMemoryRoverRepositoryMock;
let creator: RoverCreator;

beforeEach(() => {
    repository = new InMemoryRoverRepositoryMock();
    creator = new RoverCreator(repository);
});

describe('Creator should create rover', () => {
    it('should create a valid Rover', async () => {

        const id = RoverIdMother.generate();
        const coordinates = CoordinatesMother.create();
        const direction = DirectionMother.generate();

        await creator.execute(id, coordinates, direction).catch(err => { throw err });
        const rover: Rover = Rover.create(id, coordinates, direction);

        repository.assertRover(rover);
    });

    it('should return an error of repeat Rover id', async () => {
        const test = async () => {
            const id = RoverIdMother.generate();
            const coordinates = CoordinatesMother.create();
            const direction = DirectionMother.generate();
            await creator.execute(id, coordinates, direction).catch(err => { throw err });

            await creator.execute(id, coordinates, direction).catch(err => { throw err });
        };
        await expect(test())
            .rejects
            .toThrow(RoverIdAlreadyExistsException);
    });

});