import { Coordinates } from "../../../../../src/Contexts/MarsRoverMission/MarsRover/domain/Coordinates";
import { PositionMother } from "./ValueObjects/PositionMother";

export class CoordinatesMother {
    public static create(): Coordinates {
        return Coordinates.create(PositionMother.generate(), PositionMother.generate());
    }
}